from django.db import models
from django.contrib.auth.models import User


class Handbook(models.Model):
    DEVICE_TYPE_CHOICES = (
        ('Siren', 'Cирена'),
        ('Loudspeaker', 'Громкоговоритель'),
    )

    device_type = models.CharField(verbose_name='Тип устройства', choices=DEVICE_TYPE_CHOICES, max_length=11)
    address = models.TextField(verbose_name='Адрес размещения')
    latitude = models.DecimalField(verbose_name='Широта', max_digits=16, decimal_places=6)
    longitude = models.DecimalField(verbose_name='Долгота', max_digits=16, decimal_places=6)
    radius = models.IntegerField(verbose_name='Радиус покрытия')
    # user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE)

