from django.contrib import admin
from django.urls import path, include
from handbook.views import *


def index(request):
    return redirect('show_all_url')


urlpatterns = [
    path('', index),
    path('show_all/', HandbookShowAllView.as_view(), name='show_all_url'),
    path('add/', HandbookAddView.as_view(), name='add_url'),
    path('update/<int:pk>/', HandbookUpdateView.as_view(), name='update_url'),
    path('destroy/<int:pk>/', HandbookDestroyView.as_view(), name='destroy_url'),
    path('auth/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
]