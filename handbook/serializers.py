from rest_framework import serializers
from handbook.models import Handbook
from django.contrib.auth.models import User


class HandbookDetailSerializer(serializers.ModelSerializer):
    # user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Handbook
        fields = '__all__'


class HandbookListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Handbook
        fields = '__all__'