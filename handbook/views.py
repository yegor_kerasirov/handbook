from rest_framework import generics
from django.shortcuts import redirect
from handbook.serializers import *
from handbook.models import Handbook
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework import authentication
from rest_framework.permissions import IsAdminUser

import random

class HandbookShowAllView(generics.ListAPIView):
    template_name = 'handbook/show_all.html'

    def get(self, request):
        handbook = Handbook.objects.all()
        paginator = PageNumberPagination()
        context = paginator.paginate_queryset(handbook, request)
        serializer = HandbookListSerializer(context, many=True)

        paginator = paginator.get_html_context()
        return Response({
            'paginator': paginator,
            'data': serializer.data,
            'isAdmin': request.user.is_staff
        })


class HandbookAddView(generics.RetrieveAPIView,
                      generics.CreateAPIView):
    template_name = 'handbook/add.html'

    def get(self, request):
        serializer = HandbookDetailSerializer()
        return Response({
            'dir_serializer': dir(serializer),
            'serializer': serializer,
            'isAdmin': request.user.is_staff
        })

    def post(self, request):
        serializer = HandbookDetailSerializer(Handbook, data=request.data, context={'request': request})
        if not serializer.is_valid():
            return Response({'serializer': serializer})
        serializer.create(serializer.validated_data)
        return redirect('show_all_url')


class HandbookUpdateView(generics.RetrieveAPIView,
                         generics.CreateAPIView):
    permission_classes = (IsAdminUser, )
    template_name = 'handbook/update.html'

    def get(self, request, pk):
        handbook = Handbook.objects.get(pk=pk)
        serializer = HandbookDetailSerializer(handbook)
        return Response({
            'serializer': serializer,
            'data': serializer.data,
            'pk': pk,
            'isAdmin': request.user.is_staff,
        })

    def post(self, request, pk):
        handbook = Handbook.objects.get(pk=pk)
        previous_data = HandbookDetailSerializer(handbook).data
        serializer = HandbookDetailSerializer(handbook, context={'request': request}, data=request.data)

        if not serializer.is_valid():
            return Response({
                'serializer': serializer,
                'data': previous_data,
                'pk': pk,
                'isAdmin': request.user.is_staff,
            })

        serializer.save()
        return Response({
            'serializer': serializer,
            'data': serializer.data,
            'pk': pk,
            'isAdmin': request.user.is_staff,
        })


class HandbookDestroyView(generics.CreateAPIView):
    permission_classes = (IsAdminUser, )

    def post(self, request, pk):
        handbook = Handbook.objects.get(id=pk)
        handbook.delete()
        return redirect('show_all_url')


# class Check(generics.RetrieveUpdateDestroyAPIView):
#     serializer_class = HandbookListSerializer
#     queryset = Handbook.objects.all()

        # for i in range(100):
        #     types = ['Siren', 'Loudspeaker']
        #
        #     cities = ['г. Хабароск, ', 'г. Биробиджан, ', 'г. Благовещенск, ', 'г. Владивосток, ', 'г. Новосибирск, ',
        #               'г. Москва, ', 'г. Санкт-Петербург, ', 'г. Самара, ', ]
        #
        #     streets = ['ул. Ленина, ', 'ул. Советская, ', 'ул. Октябрьская, ', 'ул. Новая, ', 'ул. Трудовая, ',
        #                'ул. Тихоокеанская, ', 'ул. Карла Маркса, ', 'ул. Ким Ю Чена, ', 'ул. Мира, ', ]
        #
        #     device_type = random.choice(types)
        #     address = random.choice(cities) + random.choice(streets) + str(random.randrange(1, 200, 1))
        #     latitude = round(random.uniform(1, 99), 6)
        #     longitude = round(random.uniform(1, 99), 6)
        #     radius = random.randrange(1, 99, 1)
        #
        #     data = {'device_type': device_type, 'address': address, 'latitude': latitude,
        #             'longitude': longitude, 'radius': radius}
        #
        #     serializer = HandbookDetailSerializer(Handbook, data=data)
        #     serializer.is_valid(raise_exception=True)
        #     serializer.create(serializer.validated_data)